﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using test_task1.Models;

namespace test_task1.Controllers
{
    public class HomeController : Controller
    {
        AppDbContext context;
        public HomeController(AppDbContext context)
        {
            this.context = context;
        }

        [HttpGet("/")]
        public IActionResult Index()
        {
            return View(context.Orgs.ToList());
        }

        [HttpGet("/search")]
        public IActionResult SearchOrganisations(string searchString)
        {
            var org = from o in context.Orgs where EF.Functions.Like(o.TIN, "%" + searchString + "%") select o;
            return View("Index", org);
        }

        public IActionResult Error()
        {
            return View(new ErrorViewModel { RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier });
        }
    }
}
