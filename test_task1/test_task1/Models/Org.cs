﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace test_task1.Models
{
    public class Org
    {
        public int Id { get; set; }
        public string Name { get; set; }
        
        //taxpayer individual number
        public string TIN { get; set; }
    }
}
